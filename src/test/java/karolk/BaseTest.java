package karolk;

import org.apache.commons.codec.binary.Base32InputStream;
import org.junit.BeforeClass;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.Actions;

import java.io.IOException;
import java.util.Properties;

public class BaseTest {

    static protected RemoteWebDriver driver;

    static protected WebDriverWait wait;

    @BeforeClass
    public static void setUpDriver() throws IOException {
        Properties props = new Properties();
        props.load(BaseTest.class.getResourceAsStream("/config.properties"));
        System.out.println(props.getProperty("driverPath"));
        System.setProperty(
                "webdriver.chrome.driver",
                props.getProperty("driverPath")
        );
        driver = new ChromeDriver();

        wait = new WebDriverWait(driver, 10);
    }
}